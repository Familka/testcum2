//
//  Observable+Callable.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 07/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import RxSwift

extension Observable {
    static func fromAsync(_ handler: @escaping (@escaping (Element, Error?) -> Void) -> Void) -> Observable<Element> {
        return Observable<Element>.create { observer -> Disposable in
            handler() { result, error in
                switch (result, error) {
                case let (result, .none):
                    observer.onNext(result)
                    observer.onCompleted()
                case let (_, .some(error)):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    static func fromAsync(_ handler: @escaping (@escaping (Element) -> Void) -> Void) -> Observable<Element> {
        return fromAsync { (_handler: @escaping (Element, Error?) -> Void) in
            handler() { _handler($0, nil) }
        }
    }
    
    static func fromAsync(_ handler: @escaping (@escaping (Error?) -> Void) -> Void) -> Observable<Error> {
        return Observable<Error>.create { observer -> Disposable in
            handler() { error in
                guard let error = error else {
                    return observer.onCompleted()
                }
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
}
