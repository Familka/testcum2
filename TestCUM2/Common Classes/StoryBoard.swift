//
//  StoryBoard.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 05/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit

extension UIStoryboard {
    func instantiateViewController<T>(ofType type: T.Type) -> T {
        return instantiateViewController(withIdentifier: String(describing: type)) as! T
    }
}
