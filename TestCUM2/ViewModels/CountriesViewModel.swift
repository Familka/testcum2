//
//  CountriesViewModel.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 07/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya

final class CountriesViewModel {
    
    let disposeBag = DisposeBag()
    
    //    output
    let countriesSubject = BehaviorSubject<[Countries]>.init(value: [])
    let progressSubject = BehaviorSubject<Double>.init(value: 0)
    
    let countriesServiceLayer: CountriesServiceLayer
    
    init(service: CountriesServiceLayer = .init()) {
        countriesServiceLayer = service
        self.methodGetCountryWithProgress()
    }
    
    func methodGetCountryWithProgress() {
        countriesServiceLayer.getAllCountryWithPgoreress()
            .asDriver(onErrorJustReturn: (0, []))
            .drive(onNext: { [progressSubject, countriesSubject] (progress: Double, data: [Countries]) in
                progressSubject.onNext(progress)
                countriesSubject.onNext(data)
            }).disposed(by: disposeBag)
    }
}
