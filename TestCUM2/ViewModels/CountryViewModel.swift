//
//  CountryViewModel.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 08/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import RxSwift

final class CountryViewModel {
    
    let disposeBag = DisposeBag()
    
    /// Input
    let nameCountry: AnyObserver<String>
    
    /// Output
    let countriesServiceLayer: CountriesServiceLayer
    let country = BehaviorSubject<[String: Any]>.init(value: [:])
    
    init(countryName: String, service: CountriesServiceLayer = .init()) {
        countriesServiceLayer = service
        let currentCountry = BehaviorSubject<String>(value: countryName)
        self.nameCountry = currentCountry.asObserver()
    }
    
    func getCountry(countryName: String)  {
        countriesServiceLayer.getCountry(name: countryName).subscribe(country).disposed(by: disposeBag)
    }
}
