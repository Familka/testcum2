//
//  Router.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 05/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class Coordinator {
    
    var window: UIWindow
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    lazy var navigationViewController: UINavigationController = {
        let navigationViewController = UINavigationController()
    
        navigationViewController.navigationBar.tintColor = UIColor.white
        navigationViewController.navigationBar.barTintColor = UIColor.orange
        
        navigationViewController.navigationBar.prefersLargeTitles = true
        navigationViewController.navigationBar.largeTitleTextAttributes =  [
            NSAttributedString.Key.foregroundColor:    UIColor.white,
            NSAttributedString.Key.font:               UIFont.boldSystemFont(ofSize: 30)
        ]
        return navigationViewController
    }()
    
    init(window: UIWindow) {
        self.window = window
        self.window.makeKeyAndVisible()
        self.window.rootViewController = navigationViewController
    }
    
    func show() -> (Void) {
        guard let countriesViewController = storyboard.instantiateViewController(
            withIdentifier: "CountriesViewController") as? CountriesViewController else {
                fatalError("CountriesViewController not found") }
        self.navigationViewController.pushViewController(countriesViewController, animated: true)
    }
}

