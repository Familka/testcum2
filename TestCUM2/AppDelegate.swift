//
//  AppDelegate.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 04/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let coordinator = Coordinator(window: window!)
        coordinator.show()
        return true
    }
}

