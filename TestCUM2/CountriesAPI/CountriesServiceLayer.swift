//
//  CountriesServiceLayer.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 07/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import RxCocoa
/// Сервисный слой
///
/// getAllCountry: - Получить весь список стран и распарсить
/// getCountry: - Получить единичный объект и распарсить
struct CountriesServiceLayer {
    
    static let provider = MoyaProvider<PageRequest>()
    let disposeBag = DisposeBag()
    
    func getAllCountry() -> Observable<[Countries]> {
        return CountriesServiceLayer.provider.rx.request(.all).map { response in
            try response.map([Countries].self)
            }.asObservable()
    }
    
    func getAllCountryWithPgoreress() -> Observable<(progress: Double, data: [Countries])> {
        return CountriesServiceLayer.provider.rx.requestWithProgress(.all).map { (response: ProgressResponse) in
            let models = try response.response?.map([Countries].self) ?? []
            return (response.progress, models)
        }
    }
    
    func getCountry(name country: String) -> Observable<[String: Any]> {
        return CountriesServiceLayer.provider.rx.request(.search(country)).map { response in
            let array = try response.map([Country].self)
            var dict = try array[0].asDictionary()
            var arrayofName: [String] = []
            for i in array[0].currencies { arrayofName.append(i.name) }
            dict["currencies"] = arrayofName.joined(separator: ",")
            dict["borders"] = array[0].borders.joined(separator: ",")
            return dict
            }.asObservable()
    }
}


