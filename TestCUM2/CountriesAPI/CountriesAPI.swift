//
//  CountriesAPI.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 05/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Moya
import Foundation

/// Запросы
///
/// - all: Получить все страны
/// - search: Поиск по названию
public enum PageRequest {
    case all
    case search(String)
    
    public var baseURL: URL {
        return URL(string: "https://restcountries.eu/rest/v2")!
    }
}

extension PageRequest: TargetType {
    
    public var path: String {
        switch self {
        case .all: return "/all"
        case .search(let search): return "/name/\(search)"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .all: return .get
        case .search: return .get
        }
    }
    
    public var task: Task {
        switch self {
        case .all: return .requestPlain
        case .search(_): return .requestPlain
        }
    }
    
    public var sampleData: Data { return Data() }
    
    public var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
}
