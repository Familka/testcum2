//
//  NetworkService.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 09/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Foundation
import Alamofire
import RxCocoa
import RxSwift
import SystemConfiguration

/// Сервис проверки состояния сети
final class NetworkManager {
    
    /// Статус сети
    ///
    /// - has: Есть подключение
    /// - hasnt: Нет подключения
    enum NetworkStatus {
        case has
        case hasnt
    }
    
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    func startNetworkReachabilityObserver() -> Observable<NetworkStatus> {
        
        return Observable.create { observer -> Disposable in
            self.reachabilityManager?.listener = { status in
                switch status {
                case .notReachable:
                    print("The network is not reachable")
                    observer.onNext(NetworkStatus.hasnt)
                case .unknown :
                    print("It is unknown whether the network is reachable")
                case .reachable(.ethernetOrWiFi):
                    print("The network is reachable over the WiFi connection")
                    observer.onNext(NetworkStatus.has)
                case .reachable(.wwan):
                    print("The network is reachable over the WWAN connection")
                }
            }
            self.reachabilityManager?.startListening()
            return Disposables.create()
        }
    }
}
