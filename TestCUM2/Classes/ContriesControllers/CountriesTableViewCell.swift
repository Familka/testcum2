//
//  CountriesTableViewCell.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 05/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit

final class CountriesTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var population: UILabel!
    
    /// Конфигуратор ячейки
    ///
    /// - Parameter countries: Объект из массива
    func configure(_ countries: Countries) {
        name.text = countries.name
        population.text = String(countries.population)
    }
}
