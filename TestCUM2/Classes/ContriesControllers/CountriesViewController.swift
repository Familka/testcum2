//
//  ListCountriesViewController.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 05/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SystemConfiguration

final class CountriesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var progressView: UIProgressView!
    
    private let refreshControl = UIRefreshControl()
    private let viewModel = CountriesViewModel()
    let disposeBag = DisposeBag()
    let networkManager = NetworkManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindUI()
    }
    
    private func bindUI() {
        viewModel
            .countriesSubject.bind(
                to: tableView.rx.items(
                    cellIdentifier: CountriesTableViewCell.reuseIdentifier,
                    cellType: CountriesTableViewCell.self)) { index, model, cell in
                        cell.configure(model)
            }.disposed(by: disposeBag)
        
        viewModel.progressSubject.bind(onNext: {
            self.progressView.setProgress(Float($0), animated: true)
        }).disposed(by: disposeBag)

        tableView
            .rx.modelSelected(Countries.self)
            .subscribe {
                guard let nameCountry = $0.element?.name else { return }
                self.showDetail(name: nameCountry)
            }.disposed(by: disposeBag)
        
        refreshControl
            .rx.controlEvent(.valueChanged)
            .bind { [weak self] in
                self?.viewModel.methodGetCountryWithProgress()
                self?.refreshControl.endRefreshing()
            }.disposed(by: disposeBag)
        
        networkManager.startNetworkReachabilityObserver().bind(onNext: { status in
            switch status {
            case .has:
                print("С инетом все норм")
            case .hasnt:
                print("Потрачено Карл")
                self.showAlert()
            }
        }).disposed(by: disposeBag)
    }
    
    
    private func setupUI() {
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }

    @objc func doSomething(refreshControl: UIRefreshControl) {
        viewModel.methodGetCountryWithProgress()
        refreshControl.endRefreshing()
    }
    
    func showDetail(name country: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let countryViewController = storyboard.instantiateViewController(
            withIdentifier: "CountryViewController") as? CountryViewController else {
                fatalError("CountryViewController not found") }
        countryViewController.nameCountry = country
        self.navigationController?.pushViewController(countryViewController, animated: true)
    }
    
    func showAlert() {
        let alertController = UIAlertController(title: "Упс", message:
            "Соединение с интернетом разорвано", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Окей", style: UIAlertAction.Style.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
