//
//  CountryTableViewCell.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 08/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var title: UILabel!
    
    /// Конфигуратор
    ///
    /// - Parameters:
    ///   - country: country description
    ///   - any: any description
    func configure(name: String, detail: Any) {
        self.title.text = name
        self.detail.text = String(describing: detail)
    }
}
