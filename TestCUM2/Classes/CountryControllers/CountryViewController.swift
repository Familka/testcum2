//
//  CountryViewController.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 05/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

final class CountryViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag = DisposeBag()
    var nameCountry: String = ""
    var viewModel = CountryViewModel(countryName: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = nameCountry
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.getCountry(countryName: nameCountry)
        
        viewModel.country
            .bind(
                to: tableView.rx.items(
                    cellIdentifier: CountryTableViewCell.reuseIdentifier,
                    cellType: CountryTableViewCell.self)) { index, model, cell in
                        cell.configure(name: model.key, detail: model.value)
            }.disposed(by: disposeBag)
    }
}
