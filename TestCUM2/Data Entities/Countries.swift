//
//  Countries.swift
//  TestCUM2
//
//  Created by Фамил Гаджиев on 05/01/2019.
//  Copyright © 2019 Фамил Гаджиев. All rights reserved.
//

import Foundation

/// Города для главной страницы
struct Countries: Decodable {
    
    /// Название города
    let name: String
    
    /// Информация о количестве жителей
    let population: Int
}

extension Countries {
    private enum CodingKeys: String, CodingKey {
        case name, population
    }
}
