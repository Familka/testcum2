//
//  Currencies.swift
//  testAppPublish
//
//  Created by Фамил Гаджиев on 24/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import Foundation

/// Объект валюты
struct Currencies: Codable {
    
    /// Код валюты
    let code: String
    
    /// Название
    let name: String
    
    /// Символ
    let symbol: String
}
