//  Created by Фамил Гаджиев on 20/12/2018.
//  Copyright © 2018 Фамил Гаджиев. All rights reserved.
//

import Foundation

/// Детальная модель страны
struct Country: Codable {
    
    /// Название страны
    let name: String
    
    /// Название столицы
    let capital: String
    
    /// Количество людэ
    let population: Double
    
    /// Границы
    let borders: [String]
    
    /// Валюта
    let currencies: [Currencies]
}

extension Country {
    private enum CodingKeys: String, CodingKey {
        case name, capital, population, borders, currencies
    }
}

